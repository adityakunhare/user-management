<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'=> auth()->user()->name,
            'profile_pic' => $this->profile_picture,
            'website_link' => $this->website,
            'about' => $this->bio,
            'gallery' => $this->images,
        ];
    }
}
