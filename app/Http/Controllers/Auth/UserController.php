<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Inertia\Inertia;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $order = request()->order == null ? 'desc' : request()->order;
        return Inertia::render("Users/Index", [
            'users' =>  UserResource::collection( User::with('team')
                            ->when($request->input('search'), function ($query, $search) {
                                        $query->where('name', 'LIKE', "%{$search}%");
                            })->orderBy('id', $order)->paginate(8)->withQueryString()),
            'filters' =>  $request->only('search')
        ]);
    }

    public function create()
    {
        // $this->authorize('create', [request()->user()]);
        return Inertia::render('Users/Create', [
            'teams' => Team::orderBy('title', 'desc')->get()
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required','string','min:6'],
            'email' => ['required','email'],
            'team_id' => ['required','integer'],
            'password' => ['required','min:8']
        ]);
        User::create($data);
        return redirect('/users');
    }

    public function show(User $user)
    {
        $userData = [
            'user' => [
                'name' => $user->name,
                'email' => $user->email,
                'content' => $user->description,
                'company' => $user->organization,
            ]
        ];
        return Inertia::render('Users/Show', $userData);
    }

    public function edit(User $user)
    {
        return Inertia::render('Users/Edit', [
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'team_id' => $user->team_id,
            ],
            'teams' => Team::get()
        ]);
    }

    public function update(User $user)
    {
        $data = request()->validate([
            'name' => ['required','string','min:6'],
            'team_id' => ['required','integer'],
            'email' => ['required','email'],
        ]);
        User::where('id', $user->id)->update($data);
        return redirect('/users')
                ->with('success', 'User updated');
    }

    public function destroy(User $user)
    {
        if (request()->user()->id == $user->id) {
            return redirect('/users')
                    ->with('message', 'Ask admin to delete your profile');
        }
        User::destroy($user->id);
        return redirect('/users')
                ->with('success', 'Contact Deleted');
    }
}
