<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TeamController extends Controller
{
    public function index()
    {
        $order = request()->order == null ? 'desc': request()->order;
        return Inertia::render('Teams/Index',[
            'teams' => Team::query()
                        ->when(request()->input('search'), function($query, $search) {
                                $query->where('title', 'like',"%{$search}%");
                            })
                        ->orderBy('id', $order)
                        ->paginate(8)
                        ->withQueryString()
                        ->through(fn($team) => [
                            'id' => $team->id,
                            'title' =>  $team->title
                        ]),
            'filters' => request()->only('search'),
            'orderState' => $order
        ]);
    }

    public function create()
    {
        return Inertia::render('Teams/Create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required','string'],
            'description' => ['required','min:50']
        ]);
        Team::create($data);
        return redirect('/teams');
    }

    public function show(Team $team)
    {
        return Inertia::render('Teams/Show', [
            'team' => [
                'title' => $team->title,
                'description' => $team->description,
                'members' => $team->users,
            ],
        ]);
    }

    public function edit(Team $team)
    {
        return Inertia::render('Teams/Edit',[
            'team' => [
                'id' => $team->id,
                'title' => $team->title,
                'description' => $team->description
            ]
        ]);
    }

    public function update(Request $request, Team $team)
    {
        $data = $request->validate([
            'title' => [ 'required', 'string' ],
            'description' => ['required','min:50'],
        ]);

        $team->update($data);
        return redirect('/teams')
                ->with('success', 'Team updated');

    }

    public function destroy(Team $team)
    {
        if($team->users()->count() > 0){
            return redirect('/teams')
                    ->with('message','User is linked with respective team you are deleting');
        }
        Team::destroy($team->id);
        return redirect('/teams')
                ->with('success','Team deleted successfuly');
    }
}
