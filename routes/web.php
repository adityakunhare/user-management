<?php

    use App\Http\Controllers\Auth\LoginController;
    use App\Http\Controllers\Auth\UserController;
    use App\Http\Controllers\HomeController;
    use App\Http\Controllers\ProfileController;
    use App\Http\Controllers\SettingController;
    use App\Http\Controllers\TeamController;
    use Illuminate\Support\Facades\Route;



    Route::middleware('auth')->group(function () {
        Route::get('/', [ HomeController::class,'index' ]);

        // SETTINGS
        Route::get('/settings', [ SettingController::class,'index' ]);

        // USERS
        Route::resource('users', UserController::class);

        // TEAMS
        Route::resource('teams', TeamController::class);

        // PROFILE
        Route::get('/profile', [ProfileController::class, 'show']);
    });

    // AUTH
    Route::get('/login', [ LoginController::class,'create' ])->name('login');
    Route::post('/login', [ LoginController::class,'login' ]);
    Route::post('/logout', [ LoginController::class, 'logout' ]);
