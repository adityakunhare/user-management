<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'profile_picture' => $this->faker->image(),
            'website' => $this->faker->url,
            'bio' => $this->faker->text(),
        ];
    }
}
